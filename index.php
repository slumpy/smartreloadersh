<?php
/*
 *   Created on: Mar 20, 2015   8:11:18 PM
 *   @copyright (c) 2015, Alexander Shulzhenko,  contact@alexshulzhenko.ru
 *   @license GPL 3.0, http://opensource.org/licenses/GPL-3.0 
 */
?>

<!DOCTYPE html>
<html lang="">
    <head>
        <meta charset="utf-8" />
        <title>SmartReloaderSh</title>
        <link rel="stylesheet" href="css/style.css"/>
    </head>  
    <body>

        <div>
            <span>
                
                
            <h3>SmartReloaderSh</h3> <br/> saves your time from refreshing the page manually.
            <p>
                Change any content in this file and see how page gets autorefreshed.
                

            </p>
            
            There are two files in what you are really interested :
            <ul>
                <li>smart_reloader.php</li>
                <li>js/smart_reloader.js</li>
            </ul>
            </span>
        </div>

        
        
        <script src="//code.jquery.com/jquery-1.11.2.min.js"></script>
        <script src="js/smart_reloader.js"></script>
    </body>
</html>


